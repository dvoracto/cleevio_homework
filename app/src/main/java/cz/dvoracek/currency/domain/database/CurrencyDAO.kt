package cz.dvoracek.currency.domain.database

import androidx.room.*
import cz.dvoracek.currency.models.Currency
import kotlinx.coroutines.flow.Flow

@Dao
interface CurrencyDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(currencies: List<Currency>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateCurrency(currency: Currency)

    @Query("SELECT * from currency")
    fun getAll(): Flow<List<Currency>>

    @Query("SELECT * from currency where favourite = 1")
    fun getFavourites(): Flow<List<Currency>>

}