package cz.dvoracek.currency.domain.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cz.dvoracek.currency.models.Currency

@Database(
    entities = [
        Currency::class
    ], version = 1
)
abstract class RoomDB : RoomDatabase() {

    companion object {
        private val DATABASE_NAME = "database"

        fun createInstance(applicationContext: Context): RoomDB {
            return Room.databaseBuilder(
                applicationContext,
                RoomDB::class.java,
                DATABASE_NAME
            ).build()
        }
    }

    abstract fun currencyDAO(): CurrencyDAO
}