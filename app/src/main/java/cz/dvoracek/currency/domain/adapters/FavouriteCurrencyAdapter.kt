package cz.dvoracek.currency.domain.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import cz.dvoracek.currency.R
import cz.dvoracek.currency.databinding.ItemCurrencyBinding
import cz.dvoracek.currency.databinding.ItemFavouriteCurrencyBinding
import cz.dvoracek.currency.formatWithTwoDigits
import cz.dvoracek.currency.models.Currency

class FavouriteCurrencyAdapter constructor(
    private val context: Context,
    initialData: List<Currency> = listOf(),
    private val onFavouriteClick: (Currency) -> Unit
) : RecyclerView.Adapter<FavouriteCurrencyAdapter.FavouriteCurrencyViewHolder>() {

    private var data: MutableList<Currency> = mutableListOf()

    init {
        data = initialData.toMutableList()
    }

    fun updateDate(currencies: MutableList<Currency>) {
        val diffCallback = CurrencyDiffCallback(data, currencies)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        data.clear()
        data.addAll(currencies)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteCurrencyViewHolder {
        val binding = ItemFavouriteCurrencyBinding.inflate(LayoutInflater.from(context), parent, false)
        return FavouriteCurrencyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: FavouriteCurrencyViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class FavouriteCurrencyViewHolder constructor(
        private val binding: ItemFavouriteCurrencyBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(currency: Currency) {
            binding.rootview.setBackgroundResource(
                R.drawable.gradient_purple_grey
            )

            binding.favouriteBtn.setImageResource(
                if (currency.favourite) {
                    R.drawable.ic_favorite_white_24dp
                } else {
                    R.drawable.ic_favorite_border_white_24dp
                }
            )

            binding.favouriteCurrencyName.text = currency.name
            binding.favouriteBtn.setOnClickListener {
                onFavouriteClick.invoke(currency)
            }
        }
    }
}