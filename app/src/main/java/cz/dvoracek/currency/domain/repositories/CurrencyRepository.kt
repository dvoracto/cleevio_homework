package cz.dvoracek.currency.domain.repositories

import cz.dvoracek.currency.domain.database.RoomDB
import cz.dvoracek.currency.domain.services.CurrencyWebService
import cz.dvoracek.currency.models.Currency
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class CurrencyRepository constructor(
    private val currencyWebService: CurrencyWebService,
    private val roomDatabase: RoomDB
) {

    fun getCurrencies(): Flow<List<Currency>> {
        return flow {
            val localData = roomDatabase.currencyDAO().getAll()
            localData.collect { data ->
                if (data.isNotEmpty()) {
                    emit(data)
                } else {
                    val list = mutableListOf<Currency>()
                    currencyWebService.getCurrencies().rates.forEach {
                        list.add(Currency(0, it.key, "XX", it.value.toDouble()))
                    }

                    roomDatabase.currencyDAO().insert(list)
                    emit(list.sortedBy { currency -> currency.name })
                }
            }
        }
    }

    suspend fun updateCurrency(currency: Currency) {
        roomDatabase.currencyDAO().updateCurrency(
            currency.copy(
                favourite = !currency.favourite
            )
        )
    }

    fun getFavouriteCurrencies(): Flow<List<Currency>> {
        return roomDatabase.currencyDAO().getFavourites()
    }
}