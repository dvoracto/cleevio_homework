package cz.dvoracek.currency.domain.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import cz.dvoracek.currency.R
import cz.dvoracek.currency.databinding.ItemCurrencyBinding
import cz.dvoracek.currency.formatWithTwoDigits
import cz.dvoracek.currency.models.Currency

class CurrencyAdapter constructor(
    private val context: Context,
    initialData: List<Currency> = listOf(),
    private val selectedCurrency: (String) -> Unit,
    private val onFavouriteClick: (Currency) -> Unit
) : RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {

    private var data: MutableList<Currency> = mutableListOf()
    private var currentSelectedCurrency: String = ""

    init {
        data = initialData.toMutableList()
    }

    fun updateDate(currencies: MutableList<Currency>) {
        if (currencies.isNotEmpty() && currentSelectedCurrency.isEmpty()) {
            currentSelectedCurrency = currencies.first().name
        }

        val diffCallback = CurrencyDiffCallback(data, currencies)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        data.clear()
        data.addAll(currencies)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val binding = ItemCurrencyBinding.inflate(LayoutInflater.from(context), parent, false)
        return CurrencyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class CurrencyViewHolder constructor(
        private val binding: ItemCurrencyBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(currency: Currency) {
            binding.rootview.setBackgroundResource(
                if (currency.name == currentSelectedCurrency) {
                    R.drawable.gradient_purple_grey_selected
                } else {
                    R.drawable.gradient_purple_grey
                }
            )

            binding.favourite.setImageResource(
                if (currency.favourite) {
                    R.drawable.ic_favorite_white_24dp
                } else {
                    R.drawable.ic_favorite_border_white_24dp
                }
            )

            binding.currencyName.text = currency.name
            binding.currencySymbol.text = currency.symbol
            binding.currencyCalculatedValue.text = currency.calculatedValue.formatWithTwoDigits()
            binding.rootview.setOnClickListener {
                currentSelectedCurrency = currency.name
                selectedCurrency.invoke(currentSelectedCurrency)
                notifyDataSetChanged()
            }

            binding.favourite.setOnClickListener {
                onFavouriteClick.invoke(currency)
            }
        }
    }
}