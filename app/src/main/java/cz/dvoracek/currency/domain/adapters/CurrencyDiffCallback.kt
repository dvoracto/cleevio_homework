package cz.dvoracek.currency.domain.adapters

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import cz.dvoracek.currency.models.Currency

class CurrencyDiffCallback(
    private val oldList: List<Currency>,
    private val newList: List<Currency>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return oldList[oldPosition] == newList[newPosition] &&
                oldList[oldPosition].calculatedValue == newList[newPosition].calculatedValue
                && oldList[oldPosition].selected == newList[newPosition].selected
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}