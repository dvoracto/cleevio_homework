package cz.dvoracek.currency.domain.injection

import cz.dvoracek.currency.RetrofitHelper
import cz.dvoracek.currency.domain.database.RoomDB
import cz.dvoracek.currency.domain.repositories.CurrencyRepository
import cz.dvoracek.currency.domain.services.CurrencyWebService
import cz.dvoracek.currency.ui.currencyListFragment.CurrencyListViewModel
import cz.dvoracek.currency.ui.favouriteCurrencyFragment.FavouriteCurrencyViewModel
import cz.dvoracek.currency.ui.mainActivity.MainViewModel
import cz.dvoracek.currency.ui.randomNunberFragment.RandomNumberViewModel
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val appModule = module {

    single {
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BASIC
        }
    }
    single { RetrofitHelper.createOkHttpClient() }

    single {
        RoomDB.createInstance(
            applicationContext = androidContext()
        )
    }

    single {
        RetrofitHelper.createWebService<CurrencyWebService>(
            okHttpClient = get()
        )
    }

    single {
        CurrencyRepository(
            currencyWebService = get(),
            roomDatabase = get()
        )
    }

    single {
        MainViewModel()
    }

    single {
        CurrencyListViewModel(
            currencyRepository = get()
        )
    }

    single {
        FavouriteCurrencyViewModel(
            currencyRepository = get()
        )
    }

    single {
        RandomNumberViewModel()
    }
}