package cz.dvoracek.currency.domain.services

import cz.dvoracek.currency.models.CurrencyResponse
import retrofit2.http.GET

interface CurrencyWebService {

    @GET("latest?access_key=4ec619296267e8b9bed00632a045a519&format=1")
    suspend fun getCurrencies(): CurrencyResponse

}