package cz.dvoracek.currency

fun Double.formatWithTwoDigits(): String {
    return String.format("%.2f", this)
}