package cz.dvoracek.currency.ui.randomNunberFragment

import android.os.Bundle
import android.view.View
import cz.dvoracek.currency.MvvmFragment
import cz.dvoracek.currency.R
import kotlinx.android.synthetic.main.fragment_random_number.*

class RandomNumberFragment : MvvmFragment<RandomNumberViewModel>(
    RandomNumberViewModel::class,
    R.layout.fragment_random_number
) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        random_number_tv.text =
            resources.getString(R.string.random_number_value, viewModel.getRandomNumber())
    }
}