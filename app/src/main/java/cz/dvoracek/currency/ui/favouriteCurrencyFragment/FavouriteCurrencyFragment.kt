package cz.dvoracek.currency.ui.favouriteCurrencyFragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import cz.dvoracek.currency.MvvmFragment
import cz.dvoracek.currency.R
import cz.dvoracek.currency.domain.adapters.FavouriteCurrencyAdapter
import kotlinx.android.synthetic.main.fragment_favourite_currencies.*

class FavouriteCurrencyFragment : MvvmFragment<FavouriteCurrencyViewModel>(
    FavouriteCurrencyViewModel::class,
    R.layout.fragment_favourite_currencies
) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = FavouriteCurrencyAdapter(
            requireContext(),
            onFavouriteClick = { currency ->
                viewModel.updateCurrencyFavourite(currency)
            }
        )

        favourite_recycle_view.adapter = adapter

        viewModel.favouriteCurrencies.observe(viewLifecycleOwner, Observer {
            adapter.updateDate(it.toMutableList())
        })
    }
}