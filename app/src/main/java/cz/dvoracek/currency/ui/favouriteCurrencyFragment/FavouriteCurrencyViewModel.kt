package cz.dvoracek.currency.ui.favouriteCurrencyFragment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import cz.dvoracek.currency.domain.repositories.CurrencyRepository
import cz.dvoracek.currency.models.Currency
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class FavouriteCurrencyViewModel constructor(
    private val currencyRepository: CurrencyRepository
) : ViewModel() {

    val favouriteCurrencies = currencyRepository.getFavouriteCurrencies()
        .flowOn(Dispatchers.Default)
        .asLiveData(Dispatchers.Main)

    fun updateCurrencyFavourite(currency: Currency) {
        viewModelScope.launch(Dispatchers.Default) {
            currencyRepository.updateCurrency(currency)
        }
    }
}