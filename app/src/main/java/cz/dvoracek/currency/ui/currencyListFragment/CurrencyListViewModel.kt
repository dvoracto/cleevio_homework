package cz.dvoracek.currency.ui.currencyListFragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import cz.dvoracek.currency.domain.repositories.CurrencyRepository
import cz.dvoracek.currency.models.Currency
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class CurrencyListViewModel constructor(
    private val currencyRepository: CurrencyRepository
) : ViewModel() {

    private val currentInputValue = ConflatedBroadcastChannel(1.0)
    private val currentlySelectedCurrency = ConflatedBroadcastChannel("AED")

    val c: LiveData<List<Currency>> = combine(
        currencyRepository.getCurrencies(),
        currentInputValue.asFlow(),
        currentlySelectedCurrency.asFlow()
    ) { currencies, inputValue, selectedCurrency ->

        val selectedCurrencyRatio =
            currencies.find { it.name == selectedCurrency }?.exchangeRate
                ?: 1.0

        val newCurrencies = mutableListOf<Currency>()

        currencies.forEach { c ->
            newCurrencies.add(
                Currency(
                    c.id,
                    c.name,
                    c.symbol,
                    c.exchangeRate,
                    c.favourite
                ).apply {
                    this.selected = c.selected
                    this.calculatedValue = (inputValue / selectedCurrencyRatio) * c.exchangeRate

                }
            )
        }

        newCurrencies
    }
        .flowOn(Dispatchers.Default)
        .asLiveData(Dispatchers.Main)

    fun updateSelectedCurrency(currency: String) {
        currentlySelectedCurrency.offer(currency)
    }

    fun updateInputValue(inputValue: Double) {
        currentInputValue.offer(inputValue)
    }

    fun updateCurrencyFavourite(currency: Currency) {
        viewModelScope.launch(Dispatchers.Default) {
            currencyRepository.updateCurrency(currency)
        }
    }
}