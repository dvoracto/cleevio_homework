package cz.dvoracek.currency.ui.currencyListFragment

import android.os.Bundle
import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Observer
import cz.dvoracek.currency.MvvmFragment
import cz.dvoracek.currency.R
import cz.dvoracek.currency.domain.adapters.CurrencyAdapter
import kotlinx.android.synthetic.main.fragment_currency_list.*

class CurrencyListFragment : MvvmFragment<CurrencyListViewModel>(
    CurrencyListViewModel::class,
    R.layout.fragment_currency_list
) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = CurrencyAdapter(
            requireContext(),
            selectedCurrency = { currency ->
                viewModel.updateSelectedCurrency(currency)
            }, onFavouriteClick = { currency ->
                viewModel.updateCurrencyFavourite(currency)
            }
        )

        currency_list_recycle_view.adapter = adapter

        viewModel.c.observe(viewLifecycleOwner, Observer {
            adapter.updateDate(it.toMutableList())
        })

        currency_list_input_ed.doOnTextChanged { text, _, _, _ ->
            viewModel.updateInputValue(
                text.toString().toDoubleOrNull() ?: 1.0
            )
        }
    }
}