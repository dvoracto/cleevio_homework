package cz.dvoracek.currency.ui.randomNunberFragment

import androidx.lifecycle.ViewModel
import kotlin.random.Random

class RandomNumberViewModel : ViewModel() {

    fun getRandomNumber(): Int {
        return Random.nextInt(0, 100000)
    }
}