package cz.dvoracek.currency.ui.mainActivity

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.ui.setupActionBarWithNavController
import cz.dvoracek.currency.MvvmActivity
import cz.dvoracek.currency.R
import cz.dvoracek.currency.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : MvvmActivity<MainViewModel>(MainViewModel::class, R.layout.activity_main) {

    private var currentNavController: LiveData<NavController>? = null

    override fun onSupportNavigateUp(): Boolean {
        return currentNavController?.value?.navigateUp() ?: false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        } // Else, need to wait for onRestoreInstanceState
    }

    private fun setupBottomNavigationBar() {
        val navGraphIds = listOf(
            R.navigation.currencies,
            R.navigation.favourite,
            R.navigation.random
        )

        // Setup the bottom navigation view with a list of navigation graphs
        val controller = navigation_bar.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = R.id.nav_host_container,
            intent = intent
        )

        // Whenever the selected controller changes, setup the action bar.
        controller.observe(this, Observer { navController ->
            setupActionBarWithNavController(navController)
        })
        currentNavController = controller
    }
}
