package cz.dvoracek.currency

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import org.koin.androidx.viewmodel.ext.android.getViewModel
import kotlin.reflect.KClass

open class MvvmFragment<T : ViewModel>(
    viewModelClass: KClass<T>,
    @LayoutRes private val layout: Int
) : Fragment() {

    protected val viewModel: T by lazy {
        getViewModel(viewModelClass)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootview = inflater.inflate(layout, container, false)
        return rootview
    }
}