package cz.dvoracek.currency

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import org.koin.androidx.viewmodel.ext.android.getViewModel
import kotlin.reflect.KClass

abstract class MvvmActivity<T : ViewModel> constructor(
    viewModelClass: KClass<T>,
    @LayoutRes private val layout: Int
) : AppCompatActivity() {

    protected val viewModel: T by lazy {
        getViewModel(viewModelClass)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
    }
}