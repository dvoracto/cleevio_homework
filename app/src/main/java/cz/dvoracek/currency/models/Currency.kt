package cz.dvoracek.currency.models

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
data class Currency(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val name: String,
    val symbol: String,
    val exchangeRate: Double,
    val favourite: Boolean = false
) {
    @Ignore
    var selected: Boolean = false
    @Ignore
    var calculatedValue: Double = 0.0
}