package cz.dvoracek.currency.models

data class CurrencySelection constructor(
    val selectedCurrency: String,
    val inputValue: Double
)