package cz.dvoracek.currency.models

data class CurrencyResponse constructor(
    val rates: Map<String, String>
)